= Aufgaben
:idprefix: user-content-
:idseparator: -
:stem: latexmath
:icons: font
:hide-uri-scheme:
:sectnumlevels: 2


:sectnums:
== Multithreading-Beispiel (Vorlesung)

Welche Ausgabe erwarten Sie für folgendes Programm?

[source,java]
----
public class HelloAndGoodbye {
   public static void main(String[] args) {
      Runnable hello = () -> {
         for (int i = 1; i <= 1000; i++) {
            System.out.println("Hello " + i);
         }
      };
      Runnable goodbye = () -> {
         for (int i = 1; i <= 1000; i++) {
            System.out.println("Goodbye " + i);
         }
      };
      var t1 = new Thread(hello);
      var t2 = new Thread(goodbye);
      t1.start();
      t2.start();
   }
}
----

Führen Sie es anschliessend mehrmals aus und vergleichen Sie die Ausgaben.


== Sichtbarkeit debuggen (Vorlesung)

Verwenden Sie den Debugger in Ihrer IDE, um herauszufinden, was bei dem Programm `VisibilityProblem` schief läuft. Setzen Sie z. B. einen Breakpoint bei der `while`-Schleife und beobachten Sie das `done`-Attribut.

Beobachten Sie dasselbe Verhalten, wie wenn Sie das Programm normal (ohne Debugger) ausführen?


== Parallelisierung (Vorlesung)

Versuchen Sie das Programm `Factorization` zu _parallelisieren_, d. h. die Arbeit auf mehrere Threads aufzuteilen. Falls Sie einen Computer mit mehreren CPU-Cores haben (sehr wahrscheinlich), können die Threads ihre Arbeit «echt» gleichzeitig erledigen!

**Tipps:**

* Beginnen Sie einfach, z. B. mit 2 Threads, oder so vielen Threads, wie es Zahlen hat. Können Sie die Ausführungszeit reduzieren?
* Denken Sie daran, dass das Programm zum Messen der Ausführungszeit warten muss, bis die Threads ihre Arbeit erledigt haben.


== Hintergrund-Thread (Vorlesung)

[loweralpha]
. Ändern Sie das Programm `PlacesSearch` so ab, dass die lange dauernde `search`-Operation in einem Hintergrund-Thread statt direkt im GUI-Thread ausgeführt wird.
+
Führen Sie das Programm aus und kontrollieren Sie, ob es sich jetzt wie gewünscht verhält.

. Setzen Sie die in der Vorlesung gezeigte Technik ein, um das GUI korrekt zu aktualisieren, indem Sie die Methode `runOnUIThread` der `Application`-Klasse verwenden.
+
Leider gibt es immer noch ein Problem: Die Abfragen dauern nicht alle gleich lange; deshalb kann es passieren, dass das Resultat einer früheren Suche ein Resultat einer späteren überschreibt. Ändern Sie das Programm  nochmals ab, sodass ein Resultat nur angezeigt wird, wenn der Suchbegriff nicht schon wieder geändert wurde.
