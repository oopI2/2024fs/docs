= Ressourcen zu JWM & Skija
:stem: latexmath
:icons: font
:idprefix: user-content-
:idseparator: -

== Eine minimale JWM-App

Die https://github.com/HumbleUI/JWM[JWM]-Bibliothek stellt Klassen zum Arbeiten mit Fenstern und Events zur Verfügung. Ein minimales Beispiel, das ein leeres Fenster anzeigt:

[source,java]
----
import io.github.humbleui.jwm.*;

public class HelloJwmApp {

    public static void main(String[] args) {
        App.start(() -> { // <1>
            var window = App.makeWindow();
            window.setTitle("Hello, JWM & Skija!");
            window.setContentSize(800, 600);
            window.setEventListener(e -> { // <2>
                if (e instanceof EventWindowCloseRequest) { // <3>
                    App.terminate(); // <4>
                }
            });
            window.setVisible(true);
        });
    }
}
----
<1> Die Initialisierung (Erstellen des Fensters, usw.) kann nicht direkt in unserem Code gemacht werden, sondern muss als Lambda-Ausdruck an JWM übergeben werden (Typ `https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/lang/Runnable.html[Runnable]`).
<2> Zum Behandeln von Events braucht es einen weiteren Lambda-Ausdruck, vom Typ `https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/function/Consumer.html[Consumer]<<<Event>>>`. Wann immer ein Event ausgelöst wird, ruft JWM dieses Codestück auf.
<3> Wenn der User das Fenster zu schliessen versucht, wird das Programm nicht automatisch beendet, sondern es wird lediglich ein Event vom Typ `<<EventWindowCloseRequest>>` ausgelöst.
<4> Um das Programm zu beenden, muss `<<App>>.terminate()` aufgerufen werden.


== Fensterinhalte erstellen mit Skija

JWM ist ein reines Window-Framework, d. h. es kümmert sich nicht um den Fenster__inhalt__. Dafür brauchen wir eine separate Grafik-Bibliothek, https://github.com/HumbleUI/Skija[Skija], die aber eng mit JWM zusammenarbeitet. Ein Beispiel:

[source,java]
----
import io.github.humbleui.jwm.*;
import io.github.humbleui.jwm.skija.*;
import io.github.humbleui.skija.*;

public class HelloJwmSkijaApp {

    public static void main(String[] args) {
        new HelloJwmSkijaApp().start(); // <1>
    }

    private Window window;
    private boolean pressed;

    private void start() {
        App.start(() -> {
            window = App.makeWindow();
            window.setLayer(new LayerGLSkija()); // <2>
            window.setTitle("Hello, JWM & Skija!");
            window.setContentSize(800, 600);
            window.setEventListener(e -> {
                if (e instanceof EventKey keyEvent) {
                    handleKeyEvent(keyEvent);
                } else if (e instanceof EventFrameSkija frameEvent) { // <3>
                    draw(frameEvent.getSurface().getCanvas()); // <4>
                } else if (e instanceof EventWindowCloseRequest) {
                    App.terminate();
                }
            });
            window.setVisible(true);
        });
    }

    private void handleKeyEvent(EventKey e) {
        if (e.getKey() == Key.SPACE) {
            pressed = e.isPressed();
            window.requestFrame(); // <5>
        }
    }

    private void draw(Canvas canvas) {
        canvas.clear(0xFFFFFFFF); // <6>

        var font = new Font(Typeface.makeDefault(), 80); // <7>
        var paint = new Paint(); // <8>
        // Schriftfarbe ist abhängig von 'pressed'
        if (pressed) {
            paint.setColor(0xFFCC0000);
        } else {
            paint.setColor(0xFF333333);
        }
        canvas.drawString("Hello, JWM & Skija!", 50, 200, font, paint);
    }
}
----
<1> Für grössere Programme ist es nützlich, zu Beginn ein Objekt zu erstellen und eine Methode darauf aufzurufen, damit nicht alles `static` gemacht werden muss.
<2> Teile JWM mit, dass wir mit Skija ins Fenster zeichnen möchten...
<3> ... dadurch wird ein Event vom Typ `<<EventFrameSkija>>` ausgelöst, wann immer der Fensterinhalt gezeichnet werden soll.
<4> Das Event-Objekt stellt ein `<<Surface>>`-Objekt zur Verfügung, das wiederum ein `<<Canvas>>`-Objekt liefert. Dies ist die Hauptklasse in Skija, mit welcher alle Zeichenbefehle gemacht werden.
<5> Immer, wenn sich der sichtbare Programmzustand ändert, muss unser Code mittels `requestFrame()` dafür sorgen, dass das Fenster neu gezeichnet wird.
<6> Die Zeichenfläche von Skija ist zu Beginn vollständig schwarz. Mittels `clear(...)` kann die Zeichenfläche mit einer anderen Farbe (hier Weiss) gefüllt werden.
<7> Zum Zeichnen von Text braucht es zwei Objekte: ein `<<Typeface>>`-Objekt, das für eine Schriftart steht (z. B. _Arial_) und ein `<<Font>>`-Objekt, welches Schriftart und Schriftgrösse kombiniert.
<8> Für alle Zeichenbefehle braucht es zusätzlich ein `<<Paint>>`-Objekt, das die Füll-, bzw. Stricheigenschaften für den Zeichenbefehl definiert.


== Wichtigste Klassen & Methoden

=== Fenster & Events

Diese Klassen befinden sich im Package `io.github.humbleui.jwm`.

==== App

Einstiegsklasse für alle JWM-Applikationen.

`static start(https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/lang/Runnable.html[Runnable])`::
    Übergibt die Kontrolle an das JWM-Framework. Muss aufgerufen werden, bevor irgendeine andere Methode von JWM aufgerufen wird. Der Code, der als `Runnable` übergeben wird, kann ein Window erstellen und einen Event Handler registrieren.
`static <<Window>> makeWindow()`::
    Erstellt ein neues `<<Window>>`
`static terminate()`::
    Beendet die JWM-Applikation. Falls noch ein Fenster geöffnet ist, wird dieses geschlossen.

'''

==== Window
Repräsentiert ein Fenster, ähnlich der `Window`-Klasse aus OOP1. Fenster werden durch `<<App>>.makeWindow()` erstellt.

`setLayer(Layer)`::
    Definiert, wie die Fensteroberfläche produziert wird. Da wir als Grafikbibliothek Skija verwenden, rufen wir die Methode immer so auf: `window.setLayer(new LayerSkija());`
`setTitle(https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/lang/String.html[String])`::
    Setzt den Fenstertitel.
`setContentSize(int, int)`::
    Definiert die Grösse des Fensterinhalts. Die Angaben sind _exklusive_ Titelleiste, welche bei Windows, macOS, usw. noch dazu kommt.
`setVisible(boolean)`::
    Ein Aufruf `setVisible(true)` öffnet das Fenster. Ohne diesen Aufruf bleibt das Fenster unsichtbar.
`setEventListener(https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/function/Consumer.html[Consumer]<<<Event>>>)`::
    Definiert den Event Handler, der durch ein Callback aufgerufen wird, wenn ein Event ausgelöst wird.
`requestFrame()`::
    Teilt dem Fenster mit, dass sein Inhalt neu gezeichnet werden soll. Diese Methode muss immer aufgerufen werden, wenn sich der sichtbare Programmzustand ändert. Anschliessend wird ein entsprechender Event ausgelöst, in unserem Fall vom Typ `<<EventFrameSkija>>`.

'''

==== Event
Oberklasse von allen Events, die JWM unterstützt. Wenn ein Event ausgelöst wird, bekommt der Event Handler ein Objekt einer bestimmten Subklasse von Event, z. B. `<<EventMouseButton>>`. Mittels `instanceof` kann zwischen verschiedenen Arten von Events unterschieden werden.

'''


==== EventMouseButton
Wird ausgelöst, wenn der User eine Maustaste drückt oder loslässt.

`<<MouseButton>> getButton()`::
    Gibt die Maustaste zurück, welche gedrückt oder losgelassen wurde.
`boolean isPressed()`::
    Gibt `true` zurück, wenn die Maustaste runtergedrückt wurde, und `false`, falls die Maustaste losgelassen wurde. Wenn der User eine Taste drückt und wieder loslässt, werden nacheinander zwei separate Events ausgelöst.
`int getX()`::
    Gibt die aktuelle stem:[x]-Koordinate der Maus zurück (innerhalb des Fensterinhalts, d. h. 0 entspricht dem linken Rand des Inhalts)
`int getY()`::
    Gibt die aktuelle stem:[y]-Koordinate der Maus zurück (innerhalb des Fensterinhalts, d. h. 0 entspricht dem oberen Rand des Inhalts)

'''


==== MouseButton
Ein Enum, das die verschiedenen Maustasten repräsentiert. Im Normalfalls ist `MouseButton.PRIMARY` die linke Maustaste und `MouseButton.SECONDARY` die rechte.

'''


==== EventMouseMove
Wird ausgelöst, wenn der User die Maus innerhalb des Fensters bewegt.

`int getX()`::
    Gibt die aktuelle stem:[x]-Koordinate der Maus zurück (innerhalb des Fensterinhalts, d. h. 0 entspricht dem linken Rand des Inhalts)
`int getY()`::
    Gibt die aktuelle stem:[y]-Koordinate der Maus zurück (innerhalb des Fensterinhalts, d. h. 0 entspricht dem oberen Rand des Inhalts)

'''


==== EventKey
Wird ausgelöst, wenn der User eine Taste auf der Tastatur drückt oder loslässt.

`<<Key>> getKey()`::
    Gibt die Taste zurück, welche gedrückt oder losgelassen wurde.
`boolean isPressed()`::
    Gibt `true` zurück, wenn die Taste runtergedrückt wurde, und `false`, falls die Taste losgelassen wurde. Wenn der User eine Taste drückt und wieder loslässt, werden nacheinander zwei separate Events ausgelöst.

'''


==== Key
Ein Enum, das die verschiedenen Tasten auf der Tastur repräsentiert.Für die Buchstaben gibt es die Enum-Konstanten `Key.A` bis `Key.Z`, alle weiteren Tasten sind unter einem entsprechenden Namen ebenfalls vorhanden, z. B. `Key.SPACE`, `Key.ENTER`, `Key.LEFT`, usw.

'''


==== EventTextInput
Eine zweite Event-Art, welche ebenfalls durch Tastatureingaben ausgelöst wird.Der Unterschied zu `<<EventKey>>` besteht darin, dass diese Art von Event nur für Tasten ausgelöst wird, welche eine Textrepräsentation haben (und nicht für Pfeiltasten o.ä.) Zudem wird das Drücken der Shift-Taste bereits berücksichtigt; der zurückgegebene Text enthält also bereits korrekte Gross-/Kleinschreibung.

`https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/lang/String.html[String] getText()`::
Gibt den eingegebenen Text zurück.

'''


==== EventWindowCloseRequest
Wird ausgelöst, wenn der User das Fenster schliessen möchte, z. B. durch Drücken des X in der Titelleiste.Normalerweise sollte das Programm auf diesen Event mit `<<App>>.terminate();` reagieren.

'''


==== EventFrameSkija
Wird ausgelöst, wenn der Fensterinhalt neu gezeichnet werden muss, z. B. wenn das Fenster frisch geöffnet wurde, oder nachdem die Applikation mit `window.requestFrame()` selbst ein Neuzeichnen verlangt hat.

`<<Surface>> getSurface()`::
    Gibt die «Oberfläche» zurück, auf welche man mittels `getCanvas()` zeichnen kann.


=== Zeichnen mit Skija

Diese Klassen befinden sich im Package `io.github.humbleui.skija`.

==== Surface
Eine «Oberfläche», auf welche man zeichnen kann. In unserem Fall kommt als Oberfläche immer ein Fenster zum Zug; Skija unterstützt aber auch andere Oberflächen.

`<<Canvas>> getCanvas()`::
    Gibt den `<<Canvas>>` zurück, mit welchem man auf die Fensteroberfläche zeichnen kann.

'''


==== Canvas
Die Hauptklasse in Skija, welche verschiedene Befehle zum Zeichnen von Formen, Bildern und Text auf die Fensteroberfläche zur Verfügung stellt.

`clear(int)`::
    Füllt das ganze Fenster mit derselben Farbe. Die Farbe wird als `int`-Wert im ARGB-Format definiert. Das bedeutet, dass immer 8 Bits zusammen einem Farbkanal entsprechen; der erste Kanal «Alpha» ist die Transparenz und die folgenden drei Kanäle sind Rot, Grün und Blau. `0xFF\______` ist komplett intransparent, `0x00\______` is komplett transparent (also unsichtbar).
+
*Beispiele:*
+
[source,java]
----
int red = 0xFFFF0000;
int green = 0xFF00FF00;
int blue = 0xFF0000FF;
int gray = 0xFF999999;
----
`drawRect(<<Rect>>, <<Paint>>)`::
    Zeichnet ein Rechteck mit den gegebenen Füll- oder Stricheigenschaften.
`drawRRect(<<RRect>>, <<Paint>>)`::
    Zeichnet ein Rechteck mit gerundeten Ecken mit den gegebenen Füll- oder Stricheigenschaften.
`drawOval(<<Rect>>, <<Paint>>)`::
    Zeichnet eine Ellipse, welche durch das gegebene Rechteck umfasst wird, mit den gegebenen Füll- oder Stricheigenschaften.
`drawCircle(float x, float y, float radius, <<Paint>>)`::
    Zeichnet einen Kreis mit Mittelpunkt stem:[(x,y)], dem gegebenen Radius und den gegebenen Füll- oder Stricheigenschaften.
`drawLine(float x0, float y0, float x1, float y1, <<Paint>>)`::
    Zeichnet eine Linie mit Startpunkt stem:[(x_0,y_0)] und Endpunkt stem:[(x_1,y_1)] und den gegebenen Stricheigenschaften.
`drawString(https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/lang/String.html[String], float x, float y, <<Font>>, <<Paint>>)`::
    Zeichnet den gegebenen Text an der Stelle stem:[(x,y)] mit der gegebenen Schriftart und den gegebenen Füll- oder Stricheigenschaften. Die Koordinaten stem:[(x,y)] liegen dabei am linken Ende der https://de.wikipedia.org/wiki/Schriftlinie#/media/Datei:Typografisches_Liniensystem.svg[Grundlinie] des Texts.
`clipRect(<<Rect>>)`::
    Setzt eine sogenannte _Schnittmaske_ (_clipping mask_) in Rechteckform für die folgenden Zeichenbefehle. Diese zeichnen dadurch nur innerhalb des Rechtecks; der Bereich ausserhalb bleibt unverändert.
+
**Vorsicht:** Ohne weitere Massnahmen bleibt die Schnittmaske für alle folgenden Zeichenbefehle bestehen; um die Schnittmaske wieder zu entfernen, sollte vor dem `clipRect`-Aufruf `save()` und nach den betreffenden Operationen `restore()` aufgerufen werden.
`scale(float, float)`::
    Setzt eine Skalierungstransformation für die folgenden Zeichenbefehle. Das bedeutet, dass alle Koordinaten automatisch umgewandelt werden, z. B. indem sie um den Faktor 2 vergrössert werden. Das erste Argument setzt die Skalierung in stem:[x]-Richtung, das zweite in stem:[y]-Richtung.
+
**Vorsicht:** Wie bei `clipRect(...)` bleibt die Skalierung bestehen, bis mit `restore()` ein früherer Zustand wiederhergestellt wird.
`save()`::
    Speichert die aktuelle Schnittmaske (oder deren Abwesenheit) und die aktuelle Skalierung. Sollte vor `clipRect(...)` oder `scale(...)` aufgerufen werden.
`restore()`::
    Stellt die Schnittmaske (oder deren Abwesenheit) und die Skalierung wieder her, welche beim letzten Aufruf von `save()` gesetzt waren. Sollte nach den Zeichenbefehlen mit einer neu gesetzten Schnittmaske oder Skalierung aufgerufen werden, damit die Befehle nach `restore()` wieder mit der ursprünglichen Schnittmaske arbeiten.

'''


==== Paint
Definiert die Füll- oder Stricheigenschaften für einen Zeichenbefehl. Standardmässig füllen die meisten Zeichenbefehle die Fläche der gegebenen Form aus (z. B. `drawRect`); soll stattdessen die _Kontur_ einer Form gezeichnet werden, kann dies durch `setStroke(true)` definiert werden.

`setStroke(boolean)`::
    Falls `true` übergeben wird, wird die Fläche der zu zeichnenden Form ausgefüllt, ansonsten wird stattdessen die Kontur der Form gezeichnet. Eine Ausnahme bildet der `drawLine`-Befehl, welcher in jedem Fall die Kontur zeichnet (eine Linie hat schliesslich keine Fläche).
`setStrokeWidth(float)`::
    Setzt die Strichdicke, was aber nur einen Einfluss hat, wenn die _Kontur_ einer Form gezeichnet wird.
`setColor(int)`::
    Setzt die Füll- bzw. Strichfarbe als `int`-Wert im ARGB-Format, (siehe `<<Canvas>>.clear()`).
`setColor4f(<<Color4f>>)`::
    Setzt die Füll- bzw. Strichfarbe als `<<Color4f>>`-Objekt.

'''


==== Typeface
Eine Schriftart. Skija unterstützt das Laden von spezifischen Schriftarten, aber für unsere Anwendungen reicht vorerst die Default-Schriftart.

`static <<Typeface>> makeDefault()`::
    Erzeugt eine Default-Schriftart.

'''


==== Font
Eine Kombination von Schriftart plus Schriftgrösse. Wird zum Zeichnen von Text verwendet.

`setTypeface(<<Typeface>>)`::
    Setzt die Schriftart
`setSize(float)`::
    Setzt die Schriftgrösse
`float measureTextWidth(https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/lang/String.html[String])`::
    Gibt die Breite des gegebenen Texts zurück, wenn man ihn mit dieser Font zeichnen würde. Nützlich, falls Text zentriert oder rechtsbündig gezeichnet werden soll.
`<<FontMetrics>> getMetrics()`::
    Gibt ein Objekt zurück, welches weitere Metriken zu dieser `Font` enthält.

'''


==== FontMetrics
Enthält Metriken einer `<<Font>>`, z. B. die benötigte Höhe beim Zeichnen von Text mit dieser Font.

`float getHeight()`::
    Gibt die Höhe zurück, welche zum Zeichnen von Text mit dieser `Font` benötigt wird. Einzelne Zeichen (oder auch alle) können natürlich weniger hoch sein, aber keines ist höher.
`float getAscent()`::
    Gibt den benötigten Abstand oberhalb der https://de.wikipedia.org/wiki/Schriftlinie#/media/Datei:Typografisches_Liniensystem.svg[Grundlinie] zurück. *Achtung:* Dieser Abstand wird _negativ_ angegeben; das ist zu Beginn vielleicht unintuitiv, macht aber insofern Sinn, da «nach oben» bei GUIs ja die _negative_ stem:[y]-Richtung ist. Um die Obergrenze eines gezeichneten Texts zu erhalten, würde man z. B. `baseline + font.getAscent()` rechnen.
`float getDescent()`::
    Gibt den benötigten Abstand unterhalb der https://de.wikipedia.org/wiki/Schriftlinie#/media/Datei:Typografisches_Liniensystem.svg[Grundlinie] zurück. Um die Untergrenze eines gezeichneten Texts zu erhalten, würde man z. B. `baseline + font.getDescent()` rechnen.

'''

==== Color4f
Eine ARGB-Farbe. Objekte dieser Klasse können als Alternative zu `int`-Werten wie `0xFF999999` verwendet werden, was manchmal zu klarerem Code führt.


=== Geometrische Klassen

Diese Klassen befinden sich im Package `io.github.humbleui.types`.

==== Point

Ein Punkt mit stem:[x]- und stem:[y]-Koordinaten.

`<<Point>> offset(float dx, float dy)`:: Erzeugt eine Kopie dieses Punkts, welche um `dx`/`dy` verschoben ist.

'''

==== Rect
Ein Rechteck, das durch vier Werte definiert ist: die Positionen des linken, oberen, rechten und unteren Rands. `Rect`-Objekte werden entweder durch `new Rect(left, top, right, bottom)` oder typischer durch die statische `makeXYWH(...)`-Methode erstellt.

`static <<Rect>> makeXYWH(float left, float top, float width, float height)`::
    Erstellt ein Rechteck mit dem linken Rand an der stem:[x]-Position `left`, dem oberen Rand an der stem:[y]-Position `top` und mit der gegebenen Breite und Höhe.
`<<Rect>> inflate(float)`::
    «Bläst» das Rechteck in alle Richtungen um eine gegebene Spanne «auf» und gibt die dadurch entstandene Form zurück. Das ursprüngliche Rechteck bleibt unverändert. Falls eine positive Spanne übergeben wird, entsteht effektiv ein `RRect` (ein Rechteck mit abgerundeten Ecken), bei einer negativen Spanne hingegen ein entsprechend kleineres (normales) Rechteck.
`<<Rect>> offset(float dx, float dy)`::
    Erzeugt eine Kopie dieses Rechtecks, welche um `dx`/`dy` verschoben ist.

'''

==== IRect
Ein Rechteck, wie `<<Rect>>`, mit dem einzigen Unterschied, dass alle Grössen ganze Zahlen sind (`int` statt `float`).

'''

==== RRect
Ein Rechteck mit abgerundeten Ecken. Ist eine Subklasse von `<<Rect>>`. Alle vier Ecken können unterschiedliche Radien haben. Wird ähnlich wie `<<Rect>>` typischerweise durch `RRect.makeXYWH(...)` erstellt.

