public class RepeaterClient {
    public static void main(String[] args) {
        var repeater = new Repeater(i -> {
            System.out.println("Hello, " + i + "!");
        });
        repeater.repeat(2);

        System.out.println();
        repeater.repeat(5);
    }
}
